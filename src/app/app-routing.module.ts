import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {StoreMainComponent} from './modules/store/components/store-main/store-main.component';
import {AboutPageComponent} from './modules/about/components/about-page/about-page.component';

const routes: Routes = [
  {
    path: '',
    component: StoreMainComponent
  },
  {
    path: 'store',
    component: StoreMainComponent
  },
  {
    path: 'about',
    component: AboutPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
