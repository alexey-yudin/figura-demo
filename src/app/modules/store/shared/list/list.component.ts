import {Component, Input, OnInit} from '@angular/core';
import {Lingerie} from '../../interfaces/lingerie';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  @Input() lingerieItems: Lingerie[];

  constructor() { }

  ngOnInit(): void {
  }
}
