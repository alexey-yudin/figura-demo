import {Component, Input, OnInit} from '@angular/core';
import {Lingerie} from '../../interfaces/lingerie';

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss']
})
export class ListItemComponent implements OnInit {
  @Input() lingerie: Lingerie;

  constructor() { }

  ngOnInit(): void {
  }

}
