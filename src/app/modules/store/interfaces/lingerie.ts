export interface Lingerie {
  name: string;
  size: string;
  color: string;
}
