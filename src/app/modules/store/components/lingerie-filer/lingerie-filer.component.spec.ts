import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LingerieFilerComponent } from './lingerie-filer.component';

describe('LingerieFilerComponent', () => {
  let component: LingerieFilerComponent;
  let fixture: ComponentFixture<LingerieFilerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LingerieFilerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LingerieFilerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
