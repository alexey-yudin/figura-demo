import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-lingerie-filer',
  templateUrl: './lingerie-filer.component.html',
  styleUrls: ['./lingerie-filer.component.scss']
})
export class LingerieFilerComponent implements OnInit {
  @Output() inputChange = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  onChange(event): void {
    this.inputChange.emit(event.target.value); // Not the best approach, just for example
  }
}
