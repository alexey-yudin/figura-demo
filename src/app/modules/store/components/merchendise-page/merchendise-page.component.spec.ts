import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MerchendisePageComponent } from './merchendise-page.component';

describe('MerchendisePageComponent', () => {
  let component: MerchendisePageComponent;
  let fixture: ComponentFixture<MerchendisePageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MerchendisePageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MerchendisePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
