import { Component, OnInit } from '@angular/core';
import {Lingerie} from '../../interfaces/lingerie';

@Component({
  selector: 'app-store-main',
  templateUrl: './store-main.component.html',
  styleUrls: ['./store-main.component.scss']
})
export class StoreMainComponent implements OnInit {
  lingerieItems: Lingerie[] = [
    {
      name: 'Victoria Secret',
      size: '4A',
      color: 'black'
    },
    {
      name: 'Victoria Secret 2',
      size: '4B',
      color: 'red'
    }
  ];

  constructor() { }

  ngOnInit(): void {
  }

  onInputChange(value): void {
    console.log(value);
  }
}
