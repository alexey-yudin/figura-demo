import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LingeriePageComponent } from './lingerie-page.component';

describe('LingeriePageComponent', () => {
  let component: LingeriePageComponent;
  let fixture: ComponentFixture<LingeriePageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LingeriePageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LingeriePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
