import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreMainComponent } from './components/store-main/store-main.component';
import { ListComponent } from './shared/list/list.component';
import { ListItemComponent } from './shared/list-item/list-item.component';
import { LingerieFilerComponent } from './components/lingerie-filer/lingerie-filer.component';
import { LingeriePageComponent } from './components/lingerie-page/lingerie-page.component';
import { MerchendisePageComponent } from './components/merchendise-page/merchendise-page.component';



@NgModule({
  declarations: [
    StoreMainComponent,
    ListComponent,
    ListItemComponent,
    LingerieFilerComponent,
    LingeriePageComponent,
    MerchendisePageComponent
  ],
  exports: [
    StoreMainComponent
  ],
  imports: [
    CommonModule
  ]
})
export class StoreModule { }
